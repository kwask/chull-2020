﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Vector3 = UnityEngine.Vector3;

public class Player : MonoBehaviour
{
    public Spaceship spaceship;

    Text status_text;
    TextTimer text_timer;
    bool won = false;

    private void Awake()
    {
        GameObject timer = GameObject.FindGameObjectWithTag("TextTimer");
        text_timer = timer.GetComponent<TextTimer>();
        GameObject status = GameObject.FindGameObjectWithTag("TextStatus");
        status_text = status.GetComponent<Text>();
    }

    private void OnDestroy()
    {
        Lose("You exploded! Please try again.");
    }

    public void Lose(string reason)
    {
        if (status_text)
        {
            status_text.text = reason;
        }
        text_timer.Disable(); // stop the timer when we win
    }

    public void Win()
    {
        int capsules = 0;

        Transform[] children = GetComponentsInChildren<Transform>();
        foreach (Transform child in children)
        {
            if (child.gameObject.tag == "Cargo")
            {
                capsules++;
            }
        }

        text_timer.Disable(); // stop the timer when we win
        string time_text = text_timer.MinSecFormat();

        string text = "You escaped!\n";
        text += "Your time was " + time_text + "\n";
        text += "You escaped with " + capsules.ToString() + " cargo pods";

        status_text.text = text;
        won = true; // can't move around after winning
    }

    void FaceCursor()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Vector3 dir = hit.point - this.transform.position;
            dir.y = 0;
            
            // TODO: replace this with some angular acceleration so the player
            // cant just snap 180 degrees (PID controller?)

            this.transform.LookAt(this.transform.position + dir.normalized);
        }
    }

    private void FixedUpdate()
    {
        if(Input.touches.Length > 0)
        {
            return;
        }

        bool left_thruster = false;
        bool right_thruster = false;

        // get user input
        if (!won)
        {
            left_thruster = Input.GetKey("a");
            right_thruster = Input.GetKey("d");
        }

        // activate thrusters based on input
        spaceship.SetThrusters(left_thruster, right_thruster);
    }

    public Vector3 GetVelocity()
    {
        return spaceship.GetVelocity();
    }

    public bool HasWon()
    {
        return won;
    }
}
