﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldCharger : MonoBehaviour
{
    float power_level = 0f;
    float max_power = 1f;
    float charge_rate = 2.4f; // reappear in less than half a sec
    float max_light_intensity = 2f;

    Renderer mesh_renderer;
    Material instance_mat;
    Light shield_light;

    private void Start()
    {
        shield_light = gameObject.GetComponent<Light>();
        mesh_renderer = gameObject.GetComponent<Renderer>();
        instance_mat = mesh_renderer.material;
        UpdatePowerLevel();
    }

    // Update is called once per frame
    void Update()
    {
        power_level += charge_rate * Time.deltaTime;
        UpdatePowerLevel();
        
        // destroy this component after shield has reappeared
        if(power_level >= max_power)
        {
            Destroy(this);
        }
    }

    void UpdatePowerLevel()
    {
        if (power_level < max_power)
        {
            shield_light.intensity = power_level * max_light_intensity;
            instance_mat.SetFloat("Vector1_E70940B1", power_level);
        }
    }
}
