﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour
{
    public GameObject game_ui;
    public GameObject menu_ui;
    public GameObject tutorial_ui;

    private void Start()
    {
        Pause();
    }

    void SetMenu(bool active)
    {
        menu_ui.SetActive(active);
        game_ui.SetActive(!active);
    }

    public void StartNew()
    {
        SceneManager.LoadScene("Game");
        Pause();
    }

    public void Pause()
    {
        SetMenu(true);
        Time.timeScale = 0f;
    }

    public void Unpause()
    {
        SetMenu(false);
        tutorial_ui.SetActive(false);
        Time.timeScale = 1f;
    }

    public void Exit()
    {
        Application.Quit();
    }
}
