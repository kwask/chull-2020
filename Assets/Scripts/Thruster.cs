﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : MonoBehaviour
{
    float active_thrust = 0f;
    public Rigidbody body;
    public ParticleSystem stream;

    private void Start()
    {
        UpdateStream();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStream();
    }

    public bool Active()
    {
        return active_thrust > 0f;
    }

    public void SetThrust(float thrust)
    {
        this.active_thrust = thrust;
    }

    private void FixedUpdate()
    {
        if (Active())
        {
            VelocityUpdate();
        }
    }

    private void VelocityUpdate()
    {
        float delta_thrust = active_thrust * Time.fixedDeltaTime;
        Vector3 acceleration = transform.forward * delta_thrust;
        body.velocity += acceleration;
    }

    void UpdateStream()
    {
        if (Active() && !stream.isPlaying)
        {
            stream.Play();
        }
        else if(!Active())
        {
            stream.Stop();
        }
    }
}
