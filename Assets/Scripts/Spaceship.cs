﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    public Thruster left_thruster;
    public Thruster right_thruster;
    public Rigidbody body;

    public float max_accel = 8f;
    public float max_speed = 20f;
    public float max_angular_speed = 10f;
    public float max_angular_accel = 2f;
    public float drag_accel = 6f;

    bool left_active = false;
    bool right_active = false;
    float accel_per_thruster;
    int thruster_count = 2;

    // Start is called before the first frame update
    void Start()
    {
        accel_per_thruster = max_accel / thruster_count;
    }

    private void FixedUpdate()
    {
        float thrust = UpdateThrusters();
        if(thrust <= 0) // only if player isn't trying to move somewhere
        {
            DragUpdate();
        }
        LimitVelocity();
    }

    public void SetLeftThruster(bool active)
    {
        this.left_active = active;
    }

    public void SetRightThruster(bool active)
    {
        this.right_active = active;
    }

    public void SetThrusters(bool left, bool right)
    {
        SetLeftThruster(left);
        SetRightThruster(right);
    }

    public float UpdateThrusters()
    {
        int turning = 0;
        float left_thrust = 0f;
        float right_thrust = 0f;

        if (left_active)
        {
            turning += 1;
            left_thrust = accel_per_thruster;
        }
        if (right_active)
        {
            turning += -1;
            right_thrust = accel_per_thruster;
        }

        left_thruster.SetThrust(left_thrust);
        right_thruster.SetThrust(right_thrust);

        float ang_accel = turning * max_angular_accel;
        Turn(ang_accel);

        return left_thrust + right_thrust;
    }

    void Turn(float ang_accel)
    {
        Vector3 accel = new Vector3(0, ang_accel * Time.fixedDeltaTime, 0);

        Vector3 velocity = body.angularVelocity + accel;
        if (velocity.magnitude > max_angular_speed)
        {
            velocity = velocity.normalized * max_angular_speed;
        }
        body.angularVelocity = velocity;
    }

    void LimitVelocity()
    {
        if (System.Math.Abs(body.velocity.magnitude) > max_speed)
        {
            body.velocity = body.velocity.normalized * max_speed;
        }
    }

    public Vector3 GetVelocity()
    {
        return body.velocity;
    }

    private void DragUpdate()
    {
        float delta_drag = drag_accel * Time.fixedDeltaTime;
        body.velocity -= body.velocity.normalized * delta_drag;
        if (body.velocity.magnitude < 0.1)
        {
            body.velocity = Vector3.zero;
        }
    }
}
