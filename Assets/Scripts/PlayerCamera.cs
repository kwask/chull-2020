﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public float min_distance = 10f;
    public float min_speed_for_offset = 1f;
    public float max_accel = 0.50f;
    public float max_distance = 10f;

    // camera tracks its own velocity so that it can prevent jerky motion if
    // the player collides with something
    Vector3 velocity = Vector3.zero;
    Camera player_camera;
    Transform player_transform;
    Player player;

    // Start is called before the first frame update
    void Awake()
    {
        player_camera = GetComponent<Camera>();
        GameObject p = GameObject.FindGameObjectWithTag("Player");
        this.player_transform = p.transform;
        this.player = p.GetComponent<Player>();
    }

    private void FixedUpdate()
    {
        if(!player)
        {
            return;
        }

        Vector3 accel = this.player.GetVelocity()-velocity;
        if (accel.magnitude > max_accel)
        {
            accel = accel.normalized * max_accel;
        }
        velocity += accel;
    }

    // Update is called once per frame
    void Update()
    {
        if (!player)
        {
            return;
        }

        Vector3 velocity_offset = Vector3.zero;

        float distance = min_distance;
        float speed = velocity.magnitude;
        if (speed > min_speed_for_offset)
        {
            float max_offset = max_distance;
            speed -= min_speed_for_offset;

            float x_scale = 1 / (max_offset / 5f);
            float x_offset = -(max_offset / 2f);

            // couldn't figure out y_intercept's relationship to other vars
            float y_intercept = 0.628f;
            float y_offset = (max_offset / 2f) - y_intercept;
            float y_scale = max_offset / (float)Math.PI;

            // using atan to smoothly transition without sudden starts or stops           
            float speed_offset = y_scale * Mathf.Atan(speed * x_scale + x_offset) + y_offset;

            // zoom out the faster we move
            distance += speed_offset;
            // shift the camera in the direction of movement
            velocity_offset = speed_offset * velocity.normalized;
        }

        player_camera.orthographicSize = distance;
        Vector3 distance_offset = -this.transform.forward * distance;
        Vector3 offset = velocity_offset + distance_offset;
        this.transform.position = this.player_transform.position + offset;
    }
}
