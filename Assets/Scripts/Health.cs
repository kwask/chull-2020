﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public Vector3 shield_offset;
    public Vector3 shield_scale;
    public Vector3 shield_rotation;

    public int max_hull_health = 1;
    public float max_shield_health = 1;
    public float recharge_rate = 0.2f;
    public float min_shield_protection = 1f;

    public GameObject shield_type;
    public GameObject hit_effect_type;
    public GameObject death_effect_type;

    float last_hit_time = 0f;
    float invuln_time = 0.5f; // you get half a sec of grace between hits
    float hit_damage = 1f;

    float shield_health = 0;
    int hull_health; // permanent health, cannot restore

    GameObject shield_obj;

    void Start()
    {
        shield_health = max_shield_health;
        hull_health = max_hull_health;
        
        UpdateShieldObj();
    }

    private void Update()
    {
        UpdateShieldObj();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (this.IsDead())
        {
            Die();
            return;
        }

        shield_health += recharge_rate * Time.fixedDeltaTime;
        ClampShields();
    }

    void Die()
    {
        if(death_effect_type)
        {
            Instantiate(death_effect_type, this.transform.position, Quaternion.identity);
        }
        Destroy(this.gameObject);
    }

    public bool IsDead()
    {
        return hull_health <= 0;
    }

    void CreateShield()
    {
        shield_health = max_shield_health;
        shield_obj = Instantiate(shield_type, this.transform);
        shield_obj.transform.localPosition = shield_offset;
        shield_obj.transform.localScale = shield_scale;
        shield_obj.transform.localRotation = Quaternion.Euler(shield_rotation);
    }

    void UpdateShieldObj()
    {
        if(IsDead() || !HasShields())
        {
            Destroy(shield_obj);
            shield_obj = null;
        }else if(shield_obj == null)
        {
            CreateShield();
        }
    }

    public bool HasShields()
    {
        return shield_health >= min_shield_protection;
    }

    public void Hit(List<Vector3> hit_positions)
    {
        float current_time = Time.time;
        if(current_time-last_hit_time <= invuln_time)
        {
            return;
        }

        if(HasShields())
        {
            shield_health -= hit_damage;
            ClampShields();
        }
        else
        {
            shield_health = 0;
            hull_health -= (int)hit_damage;
        }

        if(hit_effect_type)
        {
            foreach (Vector3 pos in hit_positions)
            {
                SpawnHitEffect(pos);
            }
        }

        last_hit_time = current_time;
    }

    void ClampShields()
    {
        if(shield_health < 0)
        {
            shield_health = 0;
        }
        else if(shield_health > max_shield_health)
        {
            shield_health = max_shield_health;
        }
    }

    void SpawnHitEffect(Vector3 position)
    {
        GameObject hit_effect = Instantiate(hit_effect_type);
        hit_effect.transform.position = position;
    }
}
