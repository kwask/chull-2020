﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTimer : MonoBehaviour
{
    Text uiText;
    float prev_time = 0f;
    float start_time;
    bool active = true;

    // Start is called before the first frame update
    void Start()
    {
        uiText = GetComponent<Text>();
        Unpause();
    }

    // Update is called once per frame
    void Update()
    {
        if(uiText && active)
        {
            uiText.text = "Time: " + MinSecFormat();
        }
    }

    public float CurrentTimer()
    {
        return prev_time + Time.time - start_time;
    }

    public string MinSecFormat()
    {
        float time = CurrentTimer();
        int seconds = (int)time;
        int minutes = seconds / 60;
        seconds = seconds % 60;

        string sec_text = seconds.ToString();

        if (seconds < 10)
        {
            sec_text = "0" + sec_text;
        }

        return minutes.ToString() + ":" + sec_text;
    }

    public void Pause()
    {
        prev_time = CurrentTimer();
        Disable();
    }

    public void Unpause()
    {
        Enable();
        start_time = Time.time;
    }

    public void Enable()
    {
        active = true;
    }

    public void Disable()
    {
        active = false;    
    }
}
