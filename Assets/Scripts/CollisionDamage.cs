﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class CollisionDamage : MonoBehaviour
{
    Health health;

    public float max_safe_speed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        health = this.GetComponent<Health>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.transform.IsChildOf(this.transform))
        {
            return;
        }

        if(collision.relativeVelocity.magnitude > max_safe_speed)
        {
            List<Vector3> contact_pos = new List<Vector3>();
            foreach(ContactPoint contact in collision.contacts)
            {
                contact_pos.Add(contact.point);
            }

            health.Hit(contact_pos);
        }
    }
}
