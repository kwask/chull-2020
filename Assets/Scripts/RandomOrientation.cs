﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomOrientation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.rotation = Random.rotation;
        Destroy(this);
    }
}
