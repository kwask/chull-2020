﻿/** ===========================================================================
* author:         Nick Hirzel
* email:          hirzel.2@wright.edu
* course:         CS-3900, Game Programming
* ========================================================================== */

using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class CargoJoint : MonoBehaviour
{
    public GameObject break_effect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnJointBreak(float breakForce)
    {
        Vector3 pos = this.transform.position;
        pos += gameObject.GetComponent<Joint>().anchor;
        SpawnBreakEffect(pos);

        this.transform.parent = null;
        foreach(Joint joint in this.gameObject.GetComponentsInChildren<Joint>())
        {
            joint.transform.parent = null;
        }
    }

    void SpawnBreakEffect(Vector3 position)
    {
        GameObject effect = Instantiate(break_effect);
        effect.transform.position = position;

        Vector3 scale = new Vector3(0.15f, 0.15f, 0.15f);
        effect.transform.localScale = scale;
    }
}
