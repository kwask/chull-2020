max_speed = 20
max_offset = max_speed/2

x_scale = 1/(max_offset/5)
x_offset = -(max_offset/2)
y_offset = (max_offset / 2) - 0.628;
y_scale = max_offset / pi;

speed = [0:0.1:20]
speed_offset = y_scale * atan(speed * x_scale + x_offset) + y_offset;

plot(speed, speed_offset)
